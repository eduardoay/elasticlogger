# ElasticLogger

Simple standardized logger tool that implements python-json-logger formatter and a simple
elastic search integration.

Se the documentation [here](https://eduardoay.gitlab.io/elasticlogger-docs/index.html)
