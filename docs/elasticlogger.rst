elasticlogger package
=====================

Submodules
----------

elasticlogger.context module
----------------------------

.. automodule:: elasticlogger.context
   :members:
   :undoc-members:
   :show-inheritance:

elasticlogger.errors module
---------------------------

.. automodule:: elasticlogger.errors
   :members:
   :undoc-members:
   :show-inheritance:

elasticlogger.logger module
---------------------------

.. automodule:: elasticlogger.logger
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: elasticlogger
   :members:
   :undoc-members:
   :show-inheritance:
